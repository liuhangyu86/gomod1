package libs

func GetVal2(key string) string {
	if val, ok := gKVMap[key]; ok {
		return val
	}
	return "没有找到"
}

func SetVal2(key, val string) bool {
	gKVMap[key] = val
	return true
}

/*
单独添加标签使用库的人是无法上级高版本的

go.mod  gomod2  go.sum  main.go
liuhy@liuhy:~/go-test2/gomod2$ go build
go: finding gitlab.com/liuhangyu86/gomod1 v2.0.1
go: finding gitlab.com/liuhangyu86/gomod1 v2.0.1
go: errors parsing go.mod:
/home/liuhy/go-test2/gomod2/go.mod:3: require gitlab.com/liuhangyu86/gomod1: version "v2.0.1" invalid: module contains a go.mod file, so major version must be compatible: should be v0 or v1, not v2
liuhy@liuhy:~/go-test2/gomod2$

*/
