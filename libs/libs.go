package libs

var gKVMap map[string]string

func init() {
	gKVMap = make(map[string]string)
}

func GetVal(key string) string {
	if val, ok := gKVMap[key]; ok {
		return val
	}
	return "没有找到"
}

func SetVal(key, val string) bool {
	gKVMap[key] = val
	return true
}
